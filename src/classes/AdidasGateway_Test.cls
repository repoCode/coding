@isTest
public class AdidasGateway_Test {

    static testMethod void testHttpPost() {
        // prepare test-data
        Tournament__c tour = new Tournament__c(name = 'Spain');
        insert tour;
        insert new match__c(name = 'holanda', tournament__c = tour.Id);
    
        //As Per Best Practice it is important to instantiate the Rest Context
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/AdidasGateway'; //Request URL
        req.httpMethod = 'POST';
       	req.addParameter('identificador', 'holanda');
            
        RestContext.request = req;
        RestContext.response = res;
    
        Test.startTest();
            List<Match__c> actual = AdidasGateway.getData('holanda');
        Test.stopTest();
    
        System.assertEquals(1, actual.size(), 'Value is correct');
    }
}