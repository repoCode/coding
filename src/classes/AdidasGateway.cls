@RestResource(urlMapping='/AdidasGateway/')
global without sharing class AdidasGateway {
    
    @HttpPost
    global static List<Match__c> getData(String identificador){
        List<Match__c> dataList = [Select id, Name, Local__c, Visitor__c, Result__c from Match__c where name =: identificador];
        return dataList;
    }
}