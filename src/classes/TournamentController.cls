public class TournamentController {
    public List<Tournament__c> tourList {get; set;}
    
    public TournamentController(){
        tourList = [Select id, name, Comments__c, Year__c from Tournament__c];
    }
}